{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages."${system}";
        devShell = pkgs.mkShell {
          packages = with pkgs; [
            inkscape
            shfmt
            rnix-lsp
            nodejs
          ];
        };
      in
      {
        devShell = devShell;
        devShells.default = devShell;
      });
}
