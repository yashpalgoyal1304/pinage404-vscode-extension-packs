## V3.0.0 / 2022-11-12

-   Remove [Window Colors](https://marketplace.visualstudio.com/items?itemName=stuart.unique-window-colors) because it changes `settings.json` that does differences in git that bugs me

## V2.0.0 / 2021-10-03

-   Remove [Better Readability Extension Pack](https://marketplace.visualstudio.com/items?itemName=pinage404.better-readability-extension-pack)

## V1.0.0 / 2021-08-16

-   First major release
