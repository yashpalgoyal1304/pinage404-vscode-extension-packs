# More Missing Features of VS Code Extension Pack

Opinionated collection of extensions that fills in the missing features of VS Code (technology and language agnostic [^almost])

[![Visual Studio Marketplace Installs][vscode_marketplace_installs]][vscode_marketplace]
[![Open VSX Downloads][open_vsx_downloads]][open_vsx]

[^almost]: almost: it works with many tools but not all, yet

## Features

-   Execute code
-   Highlights todo and comments
-   Toggle quotes and (camel, kebab, snake ...)cases
-   Display errors in the code next to the faulty line
-   History so you don't lose something before you commit it

## Recommended settings

If you use `make` and you are not on Windows, add this to your settings to avoid the default [Task Explorer](https://marketplace.visualstudio.com/items?itemName=spmeesseman.vscode-taskexplorer) setting: `nmake`

```json
{
	"taskExplorer.pathToMake": "make"
}
```

If you want to keep your directories clean, add this to your settings to save history in a global hidden folder, to avoid the default behavior of [Local History](https://marketplace.visualstudio.com/items?itemName=xyz.local-history) which uses the `${workspaceFolder}` to save history in a hidden folder

```json
{
	"local-history.absolute": true,
	"local-history.path": "~/Project"
}
```

## Want to see more extension added?

Open a [MR][merge-request-url] or an [issue][issue-url] and i will to take a look

[vscode_marketplace_installs]: https://img.shields.io/visual-studio-marketplace/i/pinage404.more-missing-features-extension-pack?label=VSCode%20Marketplace%20Installs
[vscode_marketplace]: https://marketplace.visualstudio.com/items?itemName=pinage404.more-missing-features-extension-pack
[open_vsx_downloads]: https://img.shields.io/open-vsx/dt/pinage404/more-missing-features-extension-pack?label=Open%20VSX%20Registry%20Downloads
[open_vsx]: https://open-vsx.org/extension/pinage404/more-missing-features-extension-pack
[merge-request-url]: https://gitlab.com/pinage404/pinage404-vscode-extension-packs/-/merge_requests
[issue-url]: https://gitlab.com/pinage404/pinage404-vscode-extension-packs/-/issues
