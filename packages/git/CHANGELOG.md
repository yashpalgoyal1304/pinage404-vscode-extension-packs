## V2.0.0 / 2022-11-12

-   Remove [Git Web Links for VS Code](https://marketplace.visualstudio.com/items?itemName=reduckted.vscode-gitweblinks) because [GitLens — Git supercharged](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens) already do this

## V1.0.0 / 2021-08-19

-   First release
