# pinage404's Extension Packs

All pinage404's extension packs packed into one

[![Visual Studio Marketplace Installs][vscode_marketplace_installs]][vscode_marketplace]
[![Open VSX Downloads][open_vsx_downloads]][open_vsx]

[vscode_marketplace_installs]: https://img.shields.io/visual-studio-marketplace/i/pinage404.extension-packs?label=VSCode%20Marketplace%20Installs
[vscode_marketplace]: https://marketplace.visualstudio.com/items?itemName=pinage404.extension-packs
[open_vsx_downloads]: https://img.shields.io/open-vsx/dt/pinage404/extension-packs?label=Open%20VSX%20Registry%20Downloads
[open_vsx]: https://open-vsx.org/extension/pinage404/extension-packs
