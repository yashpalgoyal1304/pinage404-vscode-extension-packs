## V0.1.1 / 2023-10-13

-   Add extension [:emojisense:](https://marketplace.visualstudio.com/items?itemName=bierner.emojisense) : Adds suggestions and autocomplete for emoji

## V0.1.0 / 2021-09-12

-   First release
