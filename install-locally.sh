#!/bin/sh
cd packages/

local_extensions=$(ls --directory -- *)

for extension in $local_extensions; do
	ln --symbolic --relative $extension ~/.vscode/extensions/
done
